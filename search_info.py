import random


user_list = [["Pascal", "ROSE", "43"],
        ["Mickaël", "FLEUR", "29"],
        ["Henri", "TULIPE", "35"],
        ["Michel", "FRAMBOISE", "35"],
        ["Arthur", "PETALE", "35"],
        ["Michel", "POLLEN", "50"],
        ["Michel", "FRAMBOISE", "42"]]

# On "shuffle" la liste pour mettre tout les elements dans un ordre aléatoire
random.shuffle(user_list)




def old_user(user_list):
    for user in user_list:
        if int(user[2]) >= 50:
            print("On a dans la liste quelqu'un qui adore le camping !")

def camping_list():
    '''
    Creation de la liste des personnes qui aime le camping
    Args :
    :return:
    '''
    camping = []
    for user in user_list:
        if user[2] > str(40):
            camping.append(user)
    return camping

def camping_user(user_list):
    '''
    Resultat et affichage de la liste des personnes qui aime le camping
    Args :
    :return:
    '''
    camping = camping_list()
    for user in camping:
        print(f"Incroyable, {user[0]} {user[1]} {user[2]} ans, adore le camping !")

def prenom_nom_nb(user_list):
    '''
    Savoir le nombre le lettres dans le prénom et nom d'un personne de la liste
    Args :
    :return:
    '''
    nom_prenom = []
    for user in user_list:
        if len(user[0]) == len(user[1]):
            nom_prenom.append(user)
    return nom_prenom

def prenom_nom_nb_result(user_list):
    '''
    Affichge du resultat de la fonction pour compter le nombre de lettres dans le nom et prénom des personnes de la liste
    Args :
    :return:
    '''
    nom_prenom = prenom_nom_nb(user_list)
    for user in nom_prenom:
        print(f"Incroyable, {user[0]} {user[1]} {user[2]} a {len(user[0])} lettres dans sont prénom")
    return

def prenoms_identiques(user_list):
    '''
    Fonction pour trouver des prénoms identiques dans la liste
    :param liste:
    :return:
    '''
    doublons = []
    les_prenoms = []
    for prenom in user_list:
        les_prenoms.append(prenom[0])
    for personne in user_list:
        if les_prenoms.count(personne[0]) > 1:
            doublons.append(personne)
    return doublons

def noms_identiques(user_list):
    '''
    Fonction pour trouver des noms identique dans la liste
    :param liste:
    :return:
    '''
    doublons_nom = []
    noms=[]
    for nom in user_list :
        noms.append(nom[1])
    for personne in user_list:
        if noms.count(personne[1]) > 1:
            doublons_nom.append(personne)
    return doublons_nom

def meme_age(user_list):
    '''
    Fonction pour trouver toutes les personnes avec le meme ages dans la liste
    :param liste:
    :return:
    '''
    sans_double_age = []
    ages=[]
    for age in user_list:
        ages.append(age[2])
    for personne in user_list:
        if ages.count(personne[2]) > 1:
            sans_double_age.append(personne)
    return sans_double_age





(old_user(user_list))
print(50 * "-")
(camping_user(user_list))
print(50 * "-")
(prenom_nom_nb_result(user_list))
print(50 * "-")
print(prenoms_identiques(user_list), "on des prenoms identiques !")
print(50 * "-")
print(noms_identiques(user_list), "on des noms identiques !")
print(50 * "-")
print(meme_age(user_list), "on le meme ages !")