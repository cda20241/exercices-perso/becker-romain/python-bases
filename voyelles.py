from unidecode import unidecode

# Liste des voyelles
list_voy = ["a","e","i","o","u","y"]

def count_voy():
    # Import de la variable contenant les voyelles
    global list_voy

    # Champ de saisi utilisateur
    text = input("Ecris un mot ou une phrase : ").lower()
    print("----------")

    # Encodage UTF-8
    text_unidecode = unidecode(text)

    # Compteur de voyelles
    nb_voy = 0
    # Affichage des voyelles présentes dans la phrase ou le mot
    #final_result = [each for each in text_unidecode if each in list_voy]
    final_result = []

    # Boucle de comptage des caractères
    for letter in text_unidecode:
        if letter in list_voy:
            nb_voy += 1
    # Récupération des voyelles pour les mettres dans une liste
    for each in text_unidecode:
        if each in list_voy:
            final_result.append(each)

    # Affichage resultat
    if nb_voy == 0:
        print("Il n'y a aucune voyelle dans le mot ou la phrase ","\"" + text + "\"")
        print(121 * "=")
    elif nb_voy == 1:
        print("Il n'y a qu'une seule voyelle dans le mot ou la phrase ", "\"" + text + "\"")
        print("Liste des voyelles", final_result)
        print(121 * "=")
    else:
        print("Il y a " + str(nb_voy) + " voyelles dans le mot ou la phrase ", "\"" + text + "\"")
        print("Liste des voyelles", final_result)
        print(121 * "=")

print("")
print(49 * "=" + " Compteur de voyelles " + 50 * "=")
count_voy()