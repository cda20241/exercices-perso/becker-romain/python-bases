import random

user_list = [["Pascal", "ROSE", "43"],
        ["Mickaël", "FLEUR", "29"],
        ["Henri", "TULIPE", "35"],
        ["Michel", "FRAMBOISE", "35"],
        ["Arthur", "PETALE", "35"],
        ["Michel", "POLLEN", "50"],
        ["Michel", "FRAMBOISE", "42"]]

random.shuffle(user_list)


# Fonction pour trier par ordre alphabétique par rapport au nom
def tri_nom(user_list):
        '''
        Fonction pour trier la liste dans l'ordre alphabétique
        :param user_list: Liste a trier
        :return: user_list
        '''
        for i in range(len(user_list)):
                # Trouver le mini
                mini = i
                for j in range(i+1, len(user_list)):
                        if user_list[mini][1] > user_list[j][1]:
                                mini = j
                        elif user_list[mini][1] == user_list[j][1] and user_list[mini][0] > user_list[j][0]:
                                mini = j

                user_list[i], user_list[mini] = user_list[mini], user_list[i]

        return user_list

# Fonction pour trier par ordre alphabétique par rapport au prénom
def tri_prenom(user_list):
        '''
        Fonction pour trier la liste par prénom
        :param user_list: Liste à trier
        :return: user_list
        '''
        for i in range(len(user_list)):
                # Trouver le minimum
                mini = i

                for j in range(i + 1, len(user_list)):
                        if user_list[mini][0] > user_list[j][0]:
                                mini = j

                user_list[i], user_list[mini] = user_list[mini], user_list[i]

        return user_list


# Fonction pour trier par ordre croissant des ages
def tri_age(user_list):
        for i in range(len(user_list)):
                # Trouver le minimum
                mini = i

                for j in range(i + 1, len(user_list)):
                        if user_list[mini][2] > user_list[j][2]:
                                mini = j

                user_list[i], user_list[mini] = user_list[mini], user_list[i]

        return user_list

# Fonction de tri par rapport au nom avec les prénom utiliser comme critére de tri
def tri_nom_prenom(user_list):
        '''
        Fonction pour trier la liste par nom puis prénom sans utiliser sorted()
        :param user_list: Liste à trier
        :return: Liste triée
        '''
        for i in range(len(user_list)):
                for j in range(i + 1, len(user_list)):
                        # Comparaison par nom de famille
                        if user_list[i][1] > user_list[j][1]:
                                user_list[i], user_list[j] = user_list[j], user_list[i]
                        # En cas de noms de famille identiques, comparaison par prénom
                        elif user_list[i][1] == user_list[j][1] and user_list[i][0] > user_list[j][0]:
                                user_list[i], user_list[j] = user_list[j], user_list[i]

        return user_list


# On print le resultat de la fonction tri_nom()
# Liste des noms trier par ordre alphabétique
result_tri_nom = tri_nom(user_list)
for user in result_tri_nom:
        print(user)

print(100 * "-")

# On print le resultat de la fonction tri_prenom(user_list)
# Liste des prénoms trier par ordre croissant
result_tri_nom = tri_prenom(user_list)
for user in result_tri_nom:
        print(user)

print(50 * "-")

# On print le resultat de la fonction tri_age(user_list)
# Liste ordonnée de facon croissante par rapport au ages
result_tri_age = tri_age(user_list)
for user in result_tri_age:
        print(user)

print(50 * "-")

# On print le resultat de la fonction tri_nom_prenom(user_list)
# Liste trier par rapport au nom avec comme critére le prénom
result_nom_prenom = tri_nom_prenom(user_list)
for user in result_nom_prenom:
        print(user)