from unidecode import unidecode

symb = [ "'" , ":" , ";" , "," , "-" , "?" , "!" , " " , "." ]
def palindrome():
    global symb
    text = input("Entrez une phrase, mot ou un nombre : ").lower()

    for letter in text:
        if letter in symb:
            text = text.replace(letter, "")

    if unidecode(text.lower()) == unidecode(text[::-1].lower()):
        print("Ceci est un palindrome")
        print(112 * "=")
    else:
        print("Ceci n'est pas un palindrome")
        print(112 * "=")

print("")
print(50 * "=" + " Palindrome " + 50 * "=")
palindrome()